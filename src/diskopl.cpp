/*
 * Adplug - Replayer for many OPL2/OPL3 audio file formats.
 * Copyright (C) 1999 - 2005 Simon Peter, <dn.tlp@gmx.net>, et al.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * diskopl.cpp - Disk Writer OPL, by Simon Peter <dn.tlp@gmx.net>
 */

#include "diskopl.h"

#include <stdio.h>
#include <string.h>

// Linux headers
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()

//static const unsigned short note_table[12] = {363,385,408,432,458,485,514,544,577,611,647,686};
const unsigned char CDiskopl::op_table[9] = {0x00, 0x01, 0x02, 0x08, 0x09, 0x0a, 0x10, 0x11, 0x12};

CDiskopl::CDiskopl(std::string filename)
  : old_freq(0.0f), del(1), nowrite(false)
{
  unsigned short clock = 0xffff;

  currType = TYPE_OPL3;
  /*f = fopen(filename.c_str(),"wb");
  fwrite("RAWADATA",8,1,f);
  fwrite(&clock,sizeof(clock),1,f);*/
  f = open(filename.c_str(), O_RDWR);
  
struct termios tty;
if(tcgetattr(f, &tty) != 0) {
    printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
}
tty.c_cflag &= ~PARENB;
tty.c_cflag &= ~CSTOPB;
tty.c_cflag &= ~CSIZE;
tty.c_cflag |= CS8;
tty.c_cflag &= ~CRTSCTS;
tty.c_cflag |= CREAD | CLOCAL;
tty.c_lflag &= ~ICANON;
tty.c_lflag &= ~ECHO;
tty.c_lflag &= ~ISIG;
tty.c_iflag &= ~(IXON | IXOFF | IXANY);
tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);
tty.c_oflag &= ~OPOST;
tty.c_oflag &= ~ONLCR;
cfsetospeed(&tty, B115200);
if (tcsetattr(f, TCSANOW, &tty) != 0) {
    printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
}

}

CDiskopl::~CDiskopl()
{
  close(f);
}

void CDiskopl::update(CPlayer *p)
{
  unsigned short	clock;
  unsigned int		wait;

  if(p->getrefresh() != old_freq) {
    old_freq = p->getrefresh();
    del = wait = (unsigned int)(18.2f / old_freq);
    clock = (unsigned short)(1192737/(old_freq*(wait+1)));
    //fputc(0,f); fputc(2,f);
    //fwrite(&clock,2,1,f);
  }
  if(!nowrite) {
    //fputc(del+1,f);
    //fputc(0,f);
  }
}

void CDiskopl::setchip(int n)
{
  Copl::setchip(n);

  if(!nowrite) {
    //fputc(currChip + 1, f);
    //fputc(2, f);
  }
}

void CDiskopl::write(int reg, int val)
{
  if(!nowrite)
    diskwrite(reg,val);
}

void CDiskopl::init()
{
	unsigned char reset[1] = {0xFF};
	::write(f, reset, 1);
/*  for (int i=0;i<9;i++) {	// stop instruments
    diskwrite(0xb0 + i,0);		// key off
    diskwrite(0x80 + op_table[i],0xff);	// fastest release
  }
  diskwrite(0xbd,0);	// clear misc. register*/
}

void CDiskopl::diskwrite(int reg, int val)
{
	unsigned char buffer[3] = {0b00000000,0b01000000,0b10000000};
	buffer[0] |= reg >> 4;
	buffer[0] |= Copl::getchip() << 4;
	buffer[1] |= (reg & 0b1111) << 2;
	buffer[1] |= val >> 6;
	buffer[2] |= (val & 0b111111);

  ::write(f, buffer, 3);
  //fputc(val,f);
  //fputc(reg,f);
}
